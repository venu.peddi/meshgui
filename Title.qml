import QtQuick 2.0

Item{
    id:title

    Rectangle{
        id: headLabel
        width: parent.width-20
        height: 70
        radius: 8
        gradient: Gradient {
            GradientStop { position: 0.0; color: "#f5ffff" }
            GradientStop { position: 0.8; color: "lightskyblue"}
        }
        border{ width: 2; color: "lightgrey" }

        Text{
            id: titletext
            width: 120
            height: 40
            text: qsTr("Mesh Network GUI")
            anchors.centerIn: parent
            anchors.horizontalCenterOffset: -100
            anchors.verticalCenter: parent.verticalCenter
            font.pixelSize: 40
//            font.bold: true
            font.weight: Font.Medium
            color: "#505050"
        }
    }
}
