#include "modelpublicationstatus.h"
#include "QDebug"
#include "utilities.h"

ModelPublicationStatus::ModelPublicationStatus(QObject *parent) :
    QObject(parent)
{

}

ModelPublicationStatus::ModelPublicationStatus(QByteArray &bytes, QObject *parent) :
    QObject(parent)
{
    // Decode bytes and get the fields
    /*QByte rBytes = newmsg.Bytes();*/
    m_opcode_ModelPubStatus = getSubArray(bytes,0,16).toInt();
    m_statusCode = getSubArray(bytes, 16, 8).toUInt();
//    m_persistent = getSubArray(bytes, 24, 1).toInt();
    m_elementAddress = getSubArray(bytes, 25, 16).toUShort();


/*
    m_publicationAddress;
    m_publicAppKey{0};
    m_rfu{0};
    m_count;
    m_intervalStep;
    m_pfcFlag{false};
    m_ttlVal{255};
    m_noOfSteps{0};
    m_stepResolution;//resolution units = Sec
    m_newMsgFlag;

    m_modelIdentifier = getSubArray(bytes, , 32).toUInt();*/
}

// Property read functions
qint32 ModelPublicationStatus::opcode_ModelPubStatus(){
    return m_opcode_ModelPubStatus;
}

quint32 ModelPublicationStatus::modelIdentifier(){
    return m_modelIdentifier;
}
quint8 ModelPublicationStatus::statusCode(){
    return m_statusCode;
}

bool ModelPublicationStatus::persistent(){
    return m_persistent;
}
quint16 ModelPublicationStatus::elementAddress(){
    return m_elementAddress ;
}
quint16 ModelPublicationStatus::publicationAddress(){
    return m_publicationAddress ;
}
qint32 ModelPublicationStatus::publicAppKey(){
    return m_publicAppKey ;
}
qint32 ModelPublicationStatus::rfu(){
    return m_rfu ;
}
qint32 ModelPublicationStatus::count(){
    return m_count ;
}
qint32 ModelPublicationStatus::intervalStep(){
    return m_intervalStep ;
}
bool ModelPublicationStatus::pfcFlag(){
    return m_pfcFlag ;
}
quint8 ModelPublicationStatus::ttlVal(){
    return m_ttlVal;
}
qint32 ModelPublicationStatus::noOfSteps(){
    return m_noOfSteps;
}
qint32 ModelPublicationStatus::stepResolution (){
    return m_stepResolution  ;
}



// ////// Property write functions
void ModelPublicationStatus::setOpcode_ModelPubStatus(qint32 &opcode){
    m_opcode_ModelPubStatus = opcode;
//    qDebug() << "m_ = ";
}

void ModelPublicationStatus::setModelIdentifier(quint32 &identifier){
    m_modelIdentifier = identifier;
//    qDebug() << "m_ = ";
}

void ModelPublicationStatus::setStatusCode(quint8 &statuscode){
    m_statusCode = statuscode;
//    qDebug() << "m_ = ";
}

void ModelPublicationStatus::setPersistent(bool &persistent){
    m_persistent = persistent;
    qDebug() << "m_persistent= ";
    qDebug() << m_persistent;
}
void ModelPublicationStatus::setElementAddress(quint16 &elementAdd){
    m_elementAddress = elementAdd;
    qDebug() << "m_elementAddress = ";
    qDebug() << m_elementAddress;
}
void ModelPublicationStatus::setPublicationAddress(quint16 &publicationAdd){
    m_publicationAddress = publicationAdd;
    qDebug() << "m_publicationAddress = ";
    qDebug() << m_publicationAddress;

}
void ModelPublicationStatus::setPublicAppKey(qint32 &appKey){
    //validation
    if(appKey>=0 && appKey<=4095){
        m_publicAppKey = appKey;}
    else{
        emit invalidArgument("appKey value should be between 0 and 4095 ");
    }
    qDebug() << "m_publicAppKey = ";
    qDebug() << m_publicAppKey;
}
void ModelPublicationStatus::setCount(qint32 &count){
    if(count>=0 && count<=7){
       m_count = count;}
    else{
        emit invalidArgument("Value of the field \'Count\' should be between 0 and 7 ");
    }
    qDebug() << "m_count = ";
    qDebug() << m_count;
}
void ModelPublicationStatus::setIntervalStep(qint32 &interval){
    if(interval>+0 && interval<=31){
        m_intervalStep = interval;}
    else{
        emit invalidArgument("Value of the field \'interval\' should be between 0 and 31 ");
    }
    qDebug() << "m_intervalStep = ";
    qDebug() << m_intervalStep;
}
void ModelPublicationStatus::setPfcFlag(bool &pfc){
    m_pfcFlag = pfc;
    qDebug() << "m_pfcFlag = ";
    qDebug() << m_pfcFlag;
}

void ModelPublicationStatus::setTtlVal(quint8 &ttlVal){
    m_ttlVal = ttlVal;
    qDebug() << "m_ttlVal=";
    qDebug() << m_ttlVal;
}

void ModelPublicationStatus::setNoOfSteps(qint32 &nSteps){
    if(nSteps<=0 && nSteps>=63){
        m_noOfSteps = nSteps;}
    else{
        emit invalidArgument("Value of the field \'No Of Steps\' should be between 0 and 63 ");
    }
    qDebug() << "m_noOfSteps = ";
    qDebug() << m_noOfSteps;
}
void ModelPublicationStatus::setStepResolution (qint32 &stepRes){
    m_stepResolution =stepRes ;
    qDebug() << "m_stepResolution = ";
    qDebug() << m_stepResolution;
}



