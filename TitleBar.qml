import QtQuick 2.5

Rectangle{
    id: title
    property string title_text:"";
    width: parent.width
    height: 70
    border { width: 1; color: "lightgrey" }
    gradient: Gradient {
            GradientStop { position: 1.0; color: "#e0e0e0" }
            GradientStop { position: 0.0; color: "whitesmoke" }
    }

    Text {
        id: titleText
        x: 30
        anchors.verticalCenter: parent.verticalCenter
        text: qsTr(title_text)
        font.pixelSize: 30
 //       font.weight: Font.Medium
        color: "#505050"
    }
}
