import QtQuick 2.0
import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.4
import io.qt.examples.ModelPublicationSet 1.0


Item {
    id: modelPubSet

    ModelPublicationSet {
          id: modelPubMsg
    }

    Connections {
        target: messageHandler
        onConfigMessagesChanged: {
            console.log("QML configMessagesChangedmessage signal received");
            curPageLoader.source="ModelPublicationStatus.qml";
        }
    }

    Connections {
        target: modelPubMsg
        onSendResponse:
        {
            console.log("QML sendResponse signal received");
            messageHandler.newMsg();
        }
    }


    //page Title display
    TitleBar {
        id: titleBar
        title_text: "Model Publication Set";
    }


    //page contents display
    Rectangle{
         id: body
         x: 30
         y: titleBar.y+titleBar.height+10
         width:modelPubSet.width-10
         //height: modelPubSet.height-titleBar.height
         anchors.horizontalCenter: parent.horizontalCenter
         anchors.leftMargin: 20
         /*  Flickable {
               id: view
               anchors.fill: parent
               contentWidth: body.width
               contentHeight: 1600
       */


         Column {
             spacing: 10
             anchors.horizontalCenter: parent.horizontalCenter
             GridLayout {
                 id: layout1
                 columns: 2
                 rowSpacing: 15
                 columnSpacing: 50

               /*  anchors {
                     top: parent.top;
                     left: parent.left
                     right: parent.right
                 }
*/
                 CheckBox {
                     id:persistent
                     text: qsTr("Persistent Status")
                     checked: true
                 }
                 Label {text: ""}

                 Label {text: "Element address"; font.pixelSize: 16}
                 TextField {
                     id:elementAdd
                     height: 30
                     font.pixelSize: 14
                 }
                 Label {text: "Publication address"; font.pixelSize: 16}
                 TextField {
                     id: publicationAdd
                     height: 30
                     font.pixelSize: 14
                 }

                 Label {text: "Publick AppKey address"; font.pixelSize: 16}
                 TextField {
                     id: appKey
                     height:30
                     font.pixelSize: 14
                 }

                 CheckBox {
                     id: pfcFlag
                     text: qsTr("PublichFriendshipCredentialFlag")
                 }
                 Label { text: "" }

                 Label { text: "PublicRetransmit"; font.pixelSize:18; font.bold: true}
                 Label { text: "" }

                 Label { text: "Count" }
                 TextField {
                     id: count
                     renderType: Text.QtRendering
                 }

                 Label { text: "Interval step"}
                 TextField {
                     id: interval
                     height: 30
                     font.pixelSize: 14
                 }

                 Label { text: "Publish TTL"; font.pixelSize:18}
                 ButtonGroup {id:publishttl; buttons: column0.children}
                 Column {
                     id: column0
                     RadioButton {id:defaultTtl;checked: true; text: qsTr("DefaultTTL")}
                     RadioButton {id:publishTtl; text: qsTr("PublishTTL")}
                 }

                 Label { text: "TTL"; enabled: publishTtl.checked}
                 TextField {
                     id: ttlVal
                     height: 30
                     font.pixelSize: 14
                     text: qsTr("255")
                     enabled: publishTtl.checked
                 }

                 Label {text: qsTr("Publich Period"); font.pixelSize: 18; font.bold: true}
                 ButtonGroup {id:pp; buttons: column1.children}
                 Column {
                     id: column1
                     RadioButton {text: qsTr("PubPeriodDisabled")}
                     RadioButton {id:ppsteps;checked: true; text: qsTr("PubPeriodNoOfSteps")}
                 }
                 Label { text: "No Of Steps"; enabled: ppsteps.checked }
                 TextField {
                     id: stepsVal
                     height: 30
                     font.pixelSize: 14
                     text: qsTr("0")
                     enabled: ppsteps.checked
                 }

                 Label {text: "StepResolution"; font.pixelSize:18; }
                 ButtonGroup {
                     id: stepRes
                     buttons: column2.children
                     function text2int(txt){
                         switch(txt){
                         case "100 m.sec":
                             return 0;
                         case "10 sec":
                             return 1;
                         case "1 sec":
                             return 2;
                         case "10 min":
                             return 3;
                         default:
                             break;
                         }
                     }
                 }
                 Column { id: column2
                      RadioButton {checked: true; text: qsTr("100 m.sec")}
                      RadioButton {text: qsTr("10 sec")}
                      RadioButton {text: qsTr("1 sec")}
                      RadioButton {text: qsTr("10 min")}
                 }
             }
         }


    /*     states: State {
             name: "ShowBars"
             when: view.movingVertically
             PropertyChanges { target: verticalScrollBar; opacity: 1 }
         }


         ScrollBar {
             id: verticalScrollBar
             parent: view.parent
             width: 20; height: view.height-12
             anchors.right: view.right
             opacity: 1
             orientation: Qt.Vertical

             size: view.visibleArea.heightRatio
             position: view.visibleArea.yPosition
         }

         */

    //back button
        Rectangle {
             id: backbutton
             width: 200
             height: 40
             visible: true
             border { width: 1; color: "darkgrey"}
             gradient: Gradient {
                 GradientStop { position: 1.0; color: "lightgrey" }
                 GradientStop { position: 0.0; color: "whitesmoke" }
             }
             radius: 5
             anchors{
                 horizontalCenter: parent.horizontalCenter
                 horizontalCenterOffset:-200
                 verticalCenter: parent.verticalCenter
                 verticalCenterOffset: 700
             }
             Text {
                 id: buttonText
                 text: qsTr("back")
                 color: "black"
                 anchors.centerIn: parent
             }
             MouseArea {
                 anchors.fill: parent
                 onClicked: {
                     parent.visible= false
                     curPageLoader.source= "ConfigMessages.qml";
                 }
             }
        }

        Rectangle {
             id: sendbutton
             width: 200
             height: 40
             visible: true
             border { width: 1; color: "darkgrey"}
             gradient: Gradient {
                 GradientStop { position: 1.0; color: "lightgrey" }
                 GradientStop { position: 0.0; color: "whitesmoke" }
             }
             radius: 5
             anchors{
                 horizontalCenter: parent.horizontalCenter
                 horizontalCenterOffset:200
                 verticalCenter: parent.verticalCenter
                 verticalCenterOffset:700
             }
             Text {
                 text: qsTr("Send Message")
                 color: "black"
                 anchors.centerIn: parent
             }

             MouseArea {
                 anchors.fill: parent
                 onClicked: {
                     modelPubMsg.persistent = persistent.text;
                     modelPubMsg.elementAddress = elementAdd.text;
                     modelPubMsg.publicationAddress = publicationAdd.text;
                     modelPubMsg.publicAppKey = appKey.text;
                     modelPubMsg.count = count.text;
                     modelPubMsg.intervalStep = interval.text;
                     modelPubMsg.pfcFlag = pfcFlag.checked;
                     modelPubMsg.ttlVal = ttlVal.text;
                     modelPubMsg.noOfSteps = stepsVal.text;
                     modelPubMsg.stepResolution = stepRes.text2int(stepRes.checkedButton.text);
                     modelPubMsg.packMessage();
                 }
            }
        }
    }

}



