#include "messagehandler.h"
#include "modelpublicationset.h"
#include "QDebug"


ModelPublicationSet::ModelPublicationSet(QObject *parent) :
    QObject(parent)
{
}

// Property read functions
bool ModelPublicationSet::persistent(){
    return m_persistent;
}
quint16 ModelPublicationSet::elementAddress(){
    return m_elementAddress ;
}
quint16 ModelPublicationSet::publicationAddress(){
    return m_publicationAddress ;
}
qint32 ModelPublicationSet::publicAppKey(){
    return m_publicAppKey ;
}
qint32 ModelPublicationSet::rfu(){
    return m_rfu ;
}
qint32 ModelPublicationSet::count(){
    return m_count ;
}
qint32 ModelPublicationSet::intervalStep(){
    return m_intervalStep ;
}
bool ModelPublicationSet::pfcFlag(){
    return m_pfcFlag ;
}
//bool ModelPublicationSet::publishTTL(){
//    return m_publishTTL;
//}
quint8 ModelPublicationSet::ttlVal(){
    return m_ttlVal;
}
//bool ModelPublicationSet::publishPeriod(){
//    return m_publishPeriod;
//}
qint32 ModelPublicationSet::noOfSteps(){
    return m_noOfSteps;
}
qint32 ModelPublicationSet::stepResolution (){
    return m_stepResolution  ;
}
bool ModelPublicationSet::newMsgFlag(){
    return m_newMsgFlag;
}



// Property write functions
void ModelPublicationSet::setPersistent(bool &persistent){
    m_persistent = persistent;
    qDebug() << "m_persistent= ";
    qDebug() << m_persistent;
}
void ModelPublicationSet::setElementAddress(quint16 &elementAdd){
    m_elementAddress = elementAdd;
    qDebug() << "m_elementAddress = ";
    qDebug() << m_elementAddress;
}
void ModelPublicationSet::setPublicationAddress(quint16 &publicationAdd){
    m_publicationAddress = publicationAdd;
    qDebug() << "m_publicationAddress = ";
    qDebug() << m_publicationAddress;

}
void ModelPublicationSet::setPublicAppKey(qint32 &appKey){
    //validation
    if(appKey>=0 && appKey<=4095){
        m_publicAppKey = appKey;}
    else{
        emit invalidArgument("appKey value should be between 0 and 4095 ");
    }
    qDebug() << "m_publicAppKey = ";
    qDebug() << m_publicAppKey;
}
void ModelPublicationSet::setCount(qint32 &count){
    if(count>=0 && count<=7){
       m_count = count;}
    else{
        emit invalidArgument("Value of the field \'Count\' should be between 0 and 7 ");
    }
    qDebug() << "m_count = ";
    qDebug() << m_count;
}
void ModelPublicationSet::setIntervalStep(qint32 &interval){
    if(interval>+0 && interval<=31){
        m_intervalStep = interval;}
    else{
        emit invalidArgument("Value of the field \'interval\' should be between 0 and 31 ");
    }
    qDebug() << "m_intervalStep = ";
    qDebug() << m_intervalStep;
}
void ModelPublicationSet::setPfcFlag(bool &pfc){
    m_pfcFlag = pfc;
    qDebug() << "m_pfcFlag = ";
    qDebug() << m_pfcFlag;
}
//void ModelPublicationSet::setPublishTTL(bool &ttl){
//    m_publishTTL = ttl;
//    qDebug() << "m_publishTTL = ";
//    qDebug() << m_publishTTL;
//}
void ModelPublicationSet::setTtlVal(quint8 &ttlVal){
    m_ttlVal = ttlVal;
    qDebug() << "m_ttlVal=";
    qDebug() << m_ttlVal;
}
//void ModelPublicationSet::setPublishPeriod(bool &pp){
//    m_publishPeriod = pp;
//    qDebug() << "m_publishPeriod = ";
//    qDebug() << m_publishPeriod;
//}
void ModelPublicationSet::setNoOfSteps(qint32 &nSteps){
    if(nSteps<=0 && nSteps>=63){
        m_noOfSteps = nSteps;}
    else{
        emit invalidArgument("Value of the field \'No Of Steps\' should be between 0 and 63 ");
    }
    qDebug() << "m_noOfSteps = ";
    qDebug() << m_noOfSteps;
}
void ModelPublicationSet::setStepResolution (qint32 &stepRes){
    m_stepResolution =stepRes ;
    qDebug() << "m_stepResolution = ";
    qDebug() << m_stepResolution;
}
void ModelPublicationSet::setNewMsgFlag(bool &newMsg){
    m_newMsgFlag = newMsg;
}



//Slots


//public methods
void ModelPublicationSet::packMessage() {
//    qDebug("function reached");
    emit sendResponse();


    //MessageHandler obj;
    //messageHandler.newMsg(bytes);
}










