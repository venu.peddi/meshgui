#ifndef MODELPUBLICATIONSTATUS_H
#define MODELPUBLICATIONSTATUS_H

#include <QObject>
#include <QString>

class ModelPublicationStatus: public QObject
{
    Q_OBJECT

    Q_PROPERTY(qint32 opcode_ModelPubStatus READ opcode_ModelPubStatus WRITE setOpcode_ModelPubStatus)
    Q_PROPERTY(quint32 modelIdentifier READ modelIdentifier WRITE setModelIdentifier)
    Q_PROPERTY(quint8 statusCode READ statusCode  WRITE setStatusCode )

    Q_PROPERTY(bool persistent READ persistent WRITE setPersistent)
    Q_PROPERTY(quint16 elementAddress READ elementAddress WRITE setElementAddress)
    Q_PROPERTY(quint16 publicationAddress READ publicationAddress WRITE setPublicationAddress)
    Q_PROPERTY(qint32 publicAppKey READ publicAppKey WRITE setPublicAppKey)
    Q_PROPERTY(qint32 rfu READ rfu)
    Q_PROPERTY(qint32 count READ count WRITE setCount)
    Q_PROPERTY(qint32 intervalStep READ intervalStep WRITE setIntervalStep)
    Q_PROPERTY(bool pfcFlag READ pfcFlag WRITE setPfcFlag)
    Q_PROPERTY(quint8 ttlVal READ ttlVal WRITE setTtlVal)
    Q_PROPERTY(qint32 noOfSteps READ noOfSteps WRITE setNoOfSteps)
    Q_PROPERTY(qint32 stepResolution READ stepResolution WRITE setStepResolution)



public:
    explicit ModelPublicationStatus(QObject *parent = nullptr);
    explicit ModelPublicationStatus(QByteArray &bytes, QObject *parent = nullptr);


    // Property read functions
    qint32 opcode_ModelPubStatus();
    quint32 modelIdentifier();
    quint8 statusCode();

    bool persistent();
    quint16 elementAddress();
    quint16 publicationAddress();
    qint32 publicAppKey();
    qint32 rfu();
    qint32 count();
    qint32 intervalStep();
    bool pfcFlag();
    quint8 ttlVal();
    qint32 noOfSteps();
    qint32 stepResolution ();//resolution units are considered as Sec


    // Property write functions
    void setOpcode_ModelPubStatus(qint32 &opcode);
    void setModelIdentifier(quint32 &identifier);
    void setStatusCode(quint8 &statuscode);

    void setPersistent(bool &persistent);
    void setElementAddress(quint16 &elementAdd);
    void setPublicationAddress(quint16 &publicationAdd);
    void setPublicAppKey(qint32 &appKey);
    void setCount(qint32 &count);
    void setIntervalStep(qint32 &interval);
    void setPfcFlag(bool &pfc);
    void setPublishTTL(bool &ttl);
    void setTtlVal(quint8 &ttlVal);
    void setPublishPeriod(bool &pp);
    void setNoOfSteps(qint32 &nSteps);
    void setStepResolution (qint32 &stepRes);



signals:
    void invalidArgument(QString errStr);


public slots:


private:
    // private variables
    qint32 m_opcode_ModelPubStatus;
    quint32 m_modelIdentifier;
    quint8 m_statusCode;

    bool m_persistent{true};
    quint16 m_elementAddress;
    quint16 m_publicationAddress;
    qint32 m_publicAppKey{0};
    qint32 m_rfu{0};
    qint32 m_count;
    qint32 m_intervalStep;
    bool m_pfcFlag{false};
    quint8 m_ttlVal{255};
    qint32 m_noOfSteps{0};
    qint32 m_stepResolution;//resolution units = Sec
    bool m_newMsgFlag;

};


#endif // MODELPUBLICATIONSTATUS_H
