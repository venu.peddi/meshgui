#ifndef MESSAGEHANDLER_H
#define MESSAGEHANDLER_H

#include <QTimer>
#include <QVariant>


class MessageHandler: public QObject
{
    Q_OBJECT

    Q_PROPERTY(QVariant configMessages READ configMessages NOTIFY configMessagesChanged)


public:
    explicit MessageHandler(QObject *parent = nullptr);

    //Property Read methods
    QVariant configMessages();
    Q_INVOKABLE void newMsg();

signals:
     void configMessagesChanged();

private:
    QList<QObject*> m_configMessages;

};

#endif //MESSAGEHANDLER_H
