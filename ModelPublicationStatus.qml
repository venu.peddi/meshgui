import QtQuick 2.0
import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.4
import QtQuick.Window 2.12
//import io.qt.examples.ModelPublicationStatus 1.0
//import io.qt.examples.ModelPublication 1.0
import io.qt.examples.MessageHandler 1.0

Rectangle {
         id: msgRect
         anchors.topMargin: 30
         width: parent.width-2*x
         anchors.top: msgTitle2.bottom

         property string my_opcode_ModelPubStatus: "0"
         property string my_noOfSteps: "0"
         property string my_ttlVal: "0"
         property string my_intervalStep: "0"
         property string my_count: "0"
         property bool   my_pfcFlag: false
         property string my_publicAppKey: "0"
         property string my_publicationAddress: "0"
         property string my_persistent: "false"
         property string my_elementAdd: "0"
         property string my_statuscode: "0"

         //page Title display
    TitleBar {
             id: titleBar
             title_text: "Model Publication Status";
    }

    //page contents display
    Rectangle{
        id: body
        x: 30
        y: titleBar.y+titleBar.height+10
        width:parent.width-10
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.leftMargin: 20

     Column {
         spacing: 100
         GridLayout {
             id: layout1
             columns: 2
             rowSpacing: 10
             columnSpacing: 5
             anchors {
                 top: parent.top;
                 left: parent.left
                 right: parent.right
             }


             Label {text: "Opcode_ModelPublicationStatus";font.pixelSize: 16}
             TextField {
                   id:opcode
                   height: 30
                   font.pixelSize: 14
                   text: qsTr(my_opcode_ModelPubStatus)
             }
             Label {text: "Element address"; font.pixelSize: 16}
             TextField {
                 id:elementAdd
                 height: 30
                 font.pixelSize: 14
                 text: qsTr(my_elementAdd)
             }

             Label {text: ""}
             ButtonGroup {
                 id: sigOrVender
                 buttons: column0.children
             }
             Column { id: column0
                 RadioButton {id:sigIdSelected; checked: true; text: qsTr("ModelIdentifierSIGID")}
                 RadioButton {text: qsTr("ModelIdentifierVendorID")}
             }

             Label {text: "ModelIdentifierSIGID"; enabled: sigIdSelected.checked}
             ButtonGroup {
                id: sigId
                buttons: column1.children
             }
             Column { id: column1
                    RadioButton {checked: true; text: qsTr("ConfigurationServer(0x0000)")}
                    RadioButton {text: qsTr("ConfigurationClient(0x0001)")}
                    RadioButton {text: qsTr("HealthServer(0x0002)")}
                    RadioButton {text: qsTr("HealthClient(0x0003)")}
                    enabled: sigIdSelected.checked
             }

//             Label {text: "response_ModelPublicationGet"; font.pixelSize: 18; font.bold:true}
//             Label {text: ""}


             Label {text: "Opcode_ModelPublicationStatus";font.pixelSize: 16}
             TextField {
                   id:responseOpcode
                   height: 30
                   font.pixelSize: 14
                   text: qsTr("0x8018")
             }

            Label {text: "StatusCode"; font.pixelSize: 16}
            TextField {
                    id: statuscode
                    height: 30
                    font.pixelSize: 14
                    text:qsTr(my_statuscode)
            }

            Label {text: "Persistent"; font.pixelSize: 16}
            TextField {
                id:persistent
                height: 30
                font.pixelSize: 14
                text: qsTr(my_persistent)
                readOnly: true
            }

             Label {text: "Publication address"; font.pixelSize: 16}
             TextField {
                 id: publicationAdd
                 height: 30
                 font.pixelSize: 14
                 text: qsTr(my_publicationAddress)
                 readOnly: true
             }

             Label {text: "Publick AppKey address"; font.pixelSize: 16}
             TextField {
                 id: appKey
                 height:30
                 font.pixelSize: 14
                 text: qsTr(my_publicAppKey)
                 readOnly: true
             }


             CheckBox {
                 id: pfcFlag
                 text: qsTr("PublicFriendshipCredentialFlag")
                 checked: my_pfcFlag
                 checkable: false
             }
             Label { text: "" }

             Label { text: "PublicRetransmit"; font.pixelSize:18; font.bold: true}
             Label { text: "" }

             Label { text: "Count" }
             TextField {
                 id: count
                 renderType: Text.QtRendering
                 text: qsTr(my_count)
                 readOnly: true
             }


             Label { text: "Interval step"}
             TextField {
                 id: interval
                 height: 30
                 font.pixelSize: 14
                 text: qsTr(my_intervalStep)
                 readOnly: true
             }


             Label { text: "Publish TTL"; font.pixelSize:18}
             ButtonGroup {id:publishttl; buttons: column2.children}
             Column {
                 id: column2
                 RadioButton {id:defaultTtl;checked: true; text: qsTr("DefaultTTL")}
                 RadioButton {id:publishTtl; text: qsTr("PublishTTL")}
             }

             Label { text: "TTL"; enabled: publishTtl.checked}
             TextField {
                 id: ttlVal
                 height: 30
                 font.pixelSize: 14
                 text: qsTr(my_ttlVal)
                 readOnly: true
                 enabled: publishTtl.checked
             }

             Label {text: qsTr("Publich Period"); font.pixelSize: 18; font.bold: true}
             ButtonGroup {id:pp; buttons: column3.children}
             Column {
                 id: column3
                 RadioButton {text: qsTr("PubPeriodDisabled")}
                 RadioButton {id:ppsteps;checked: true; text: qsTr("PubPeriodNoOfSteps")}
             }
             Label { text: "No Of Steps"; enabled: ppsteps.checked }
             TextField {
                 id: stepsVal
                 height: 30
                 font.pixelSize: 14
                 text: qsTr(my_noOfSteps)
                 readOnly: true
                 enabled: ppsteps.checked
             }

             Label {text: "StepResolution"; font.pixelSize:18; }
             ButtonGroup {
                 id: stepRes
                 buttons: column2.children
             }
             Column { id: column4
                  RadioButton {checked: true; text: qsTr("100 m.sec")}
                  RadioButton {text: qsTr("10 sec")}
                  RadioButton {text: qsTr("1 sec")}
                  RadioButton {text: qsTr("10 min")}
             }
         }
     }


     Rectangle {
         id: backbutton
         width: 200
         height: 40
         visible: true
         border { width: 1; color: "darkgrey"}
         gradient: Gradient {
             GradientStop { position: 1.0; color: "lightgrey" }
             GradientStop { position: 0.0; color: "whitesmoke" }
         }
         radius: 5
         anchors{
             horizontalCenter: parent.horizontalCenter
             horizontalCenterOffset:-200
             verticalCenter: parent.verticalCenter
             verticalCenterOffset: 700
         }
         Text {
             id: buttonText
             text: qsTr("back")
             color: "black"
             anchors.centerIn: parent
         }
         MouseArea {
             anchors.fill: parent
             onClicked: {
                 parent.visible= false
                 curPageLoader.source= "ModelPublicationSet.qml";
             }
         }
    }
}
}
