import QtQuick 2.12

Rectangle {
              id: button
               width: 200
               height: 40
               visible: true
               border { width: 1; color: "darkgrey"}
               gradient: Gradient {
                   GradientStop { position: 1.0; color: "lightgrey" }
                   GradientStop { position: 0.0; color: "whitesmoke" }
               }
               radius: 5
               anchors{
                     horizontalCenter: parent.horizontalCenter
                     horizontalCenterOffset:300
                     verticalCenter: parent.verticalCenter
                     verticalCenterOffset:300
               }
               Text {
                   id: buttonText
                   text: qsTr("parent.text")
                   color: "black"
                   anchors.centerIn: parent
               }
               MouseArea {
                   anchors.fill: parent
               }
}



