#include "messagehandler.h"
#include "modelpublicationstatus.h"
#include "utilities.h"
#include "QDebug"


MessageHandler::MessageHandler(QObject *parent):
    QObject(parent)
{

}

QVariant MessageHandler::configMessages(){
    return QVariant::fromValue(m_configMessages);
}


void MessageHandler::newMsg() {
    QByteArray bytes;
    bytes.append(0x80);
    bytes.append(0x18);
    bytes.append(0x01);
    bytes.append(0x11);


    // Figure out message type and then construct the appropriate message class with bytes as argument
    qDebug() << "newMsg method reached";
    QByteArray tmp_opcode = getSubArray(bytes,0,2).toHex();
    qDebug() << tmp_opcode.toInt();
    switch(tmp_opcode.toInt()){
    case 8018:
    case 8019:
    case 8010:
           ModelPublicationStatus *newMsg = new ModelPublicationStatus(bytes);
           m_configMessages.append(newMsg);
           emit configMessagesChanged();
           qDebug() << "CPP configMessagesChanged signal emitted";
           break;
    }
/*    tmp_opcode = getSubArray(bytes,0,8).toInt();
    switch(tmp_opcode){
    case 0x02:
           //Opcode_CompositionDataStatus *newMsg = new Opcode_CompositionDataStatus(bytes);
           break;
    case 0x06:
           //Opcode_HeartbeatPublicationStatus *newMsg = new Opcode_HeartbeatPublicationStatus(bytes);
           break;
    }

*/
}



