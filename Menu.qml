import QtQuick 2.12
import QtQuick.Window 2.12


Item{
     id: menu
     //page Title display
     TitleBar {
         id: titleBar
         title_text: "Model List";
     }

     //page contents display
     Rectangle{
          id: body
          x: 30
          y: titleBar.y+titleBar.height+10
          width:parent.width-10
          anchors.horizontalCenter: parent.horizontalCenter

          //property alias titletext: "Menu";
          Column {
              anchors.horizontalCenter: parent.horizontalCenter
              Repeater {
                  model: ["ConfigMessages"]
                  Rectangle{
                      id: title
                      width: menu.width-5
                      height: 70
                      border{width: 1; color: "lightgrey" }
                      radius: 10
                      gradient: Gradient {
                          GradientStop { position: 1.0; color: "#f0f0f0" }
                          GradientStop { position: 0.0; color: "white" }
                      }

                      Text {
                          id: menuTitleText
                          x: 30
                          anchors.verticalCenter: parent.verticalCenter
                          anchors.horizontalCenter: parent.horizontalCenter
                          text: qsTr(modelData)
                          font.pixelSize: 28
                          font.weight:Font.Light
                          color: "#505050"
                       }

                       MouseArea {
                           anchors.fill: parent
                           onClicked: curPageLoader.source = modelData+".qml";
                       }
                  }
              }
          }
     }
}
