#ifndef MODELPUBLICATION_H
#define MODELPUBLICATION_H


#include <QObject>
#include <QString>

class MessageHandler;
class ModelPublicationSet: public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool persistent READ persistent WRITE setPersistent)
    Q_PROPERTY(quint16 elementAddress READ elementAddress WRITE setElementAddress)
    Q_PROPERTY(quint16 publicationAddress READ publicationAddress WRITE setPublicationAddress)
    Q_PROPERTY(qint32 publicAppKey READ publicAppKey WRITE setPublicAppKey)
    Q_PROPERTY(qint32 rfu READ rfu)
    Q_PROPERTY(qint32 count READ count WRITE setCount)
    Q_PROPERTY(qint32 intervalStep READ intervalStep WRITE setIntervalStep)
    Q_PROPERTY(bool pfcFlag READ pfcFlag WRITE setPfcFlag)
    Q_PROPERTY(quint8 ttlVal READ ttlVal WRITE setTtlVal)
    Q_PROPERTY(qint32 noOfSteps READ noOfSteps WRITE setNoOfSteps)
    Q_PROPERTY(qint32 stepResolution READ stepResolution WRITE setStepResolution)


public:
     explicit ModelPublicationSet(QObject *parent = nullptr);

    //methods
    Q_INVOKABLE void packMessage();


    // Property read functions
    bool persistent();
    quint16 elementAddress();
    quint16 publicationAddress();
    qint32 publicAppKey();
    qint32 rfu();
    qint32 count();
    qint32 intervalStep();
    bool pfcFlag();
    quint8 ttlVal();
    qint32 noOfSteps();
    qint32 stepResolution ();//resolution units are considered as Sec
    bool newMsgFlag();


    // Property write functions
    void setPersistent(bool &persistent);
    void setElementAddress(quint16 &elementAdd);
    void setPublicationAddress(quint16 &publicationAdd);
    void setPublicAppKey(qint32 &appKey);
    void setCount(qint32 &count);
    void setIntervalStep(qint32 &interval);
    void setPfcFlag(bool &pfc);
    void setPublishTTL(bool &ttl);
    void setTtlVal(quint8 &ttlVal);
    void setPublishPeriod(bool &pp);
    void setNoOfSteps(qint32 &nSteps);
    void setStepResolution (qint32 &stepRes);
    void setNewMsgFlag(bool &newMsg);



signals:
    void invalidArgument(QString errStr);
    // FIXME: This is only for testing the response without backend
    void sendResponse();

public slots:


private:
    // private variables
    bool m_persistent{true};
    quint16 m_elementAddress;
    quint16 m_publicationAddress;
    qint32 m_publicAppKey{0};
    qint32 m_rfu{0};
    qint32 m_count;
    qint32 m_intervalStep;
    bool m_pfcFlag{false};
    quint8 m_ttlVal{255};
    qint32 m_noOfSteps{0};
    qint32 m_stepResolution;//resolution units = Sec
    bool m_newMsgFlag;

};


#endif // MODELPUBLICATION_H
