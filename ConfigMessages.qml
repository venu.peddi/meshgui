import QtQuick 2.12
import QtQuick.Window 2.12


Item{
     id: configMsgs

     //page Title display
     TitleBar {
         id: titleBar
         title_text: "Configuration Messages";
     }

     //page contents display
     Rectangle{
          id: body
          x: 30
          y: titleBar.y+titleBar.height+10
          width:parent.width-10
          anchors.horizontalCenter: parent.horizontalCenter


          Column {
              anchors.horizontalCenter: parent.horizontalCenter
              Repeater {
                  model: ["ModelPublicationSet", "ModelPublicationGet"]
                  Rectangle{
                      id: msg1
                      width: configMsgs.width-5
                      height: 70

                      border { width: 1; color: "lightgrey" }
                      radius: 10
                      gradient: Gradient {
                          GradientStop { position: 1.0; color: "#f0f0f0" }
                          GradientStop { position: 0.0; color: "white" }
                      }
                      Text {
                          id: text1
                          x: 30
                          anchors.verticalCenter: parent.verticalCenter
                          anchors.horizontalCenter: parent.horizontalCenter
                          text: qsTr(modelData)
                          font.pixelSize: 28
                          font.weight: Font.Light
                          color: "#505050"
                      }

                      MouseArea {
                          anchors.fill: parent
                          onClicked: curPageLoader.source=modelData+".qml";
                      }
                  }
              }
          }
     }


     Rectangle {
          id: backbutton
          width: 200
          height: 40
          visible: true
          border { width: 1; color: "darkgrey"}
          gradient: Gradient {
              GradientStop { position: 1.0; color: "lightgrey" }
              GradientStop { position: 0.0; color: "whitesmoke" }
          }
          radius: 5
          anchors{
              horizontalCenter: parent.horizontalCenter
              verticalCenter: parent.verticalCenter
              verticalCenterOffset:400
          }
          Text {
              id: buttonText
              text: qsTr("back")
              color: "black"
              anchors.centerIn: parent
          }
          MouseArea {
              anchors.fill: parent
              onClicked: {
                  parent.visible= false
                  curPageLoader.source= "Menu.qml";
              }
          }
     }
}

