import QtQuick 2.5

Item{
    id: app
    anchors.fill: parent
    opacity: 0


    function init()
    {
        opacity = 1.0
        curPageLoader.setSource("Menu.qml")
    }

    Rectangle{
        id: baseRect
        x:5
        y:5
        width: parent.width-10
        height: parent.height-10
        radius:10

        //GUI Main Title display rectangle
        Rectangle{
             id: mainTitle
             x: 5
             y: 5
             width:parent.width+10
             Loader{
                 id: titleLoader
                 anchors.fill: parent
                 source: "Title.qml"
             }
        }

        Rectangle{
            id: viewPage
            x: 5
            y: 100
            width: parent.width-10
            height: parent.height-y-5
            color: "#f9f9f9"
            border { width: 2; color: "lightgrey" }

            Loader {
                    id: curPageLoader
                    anchors.fill: parent

                    onStatusChanged: {
                        if (status === Loader.Ready)
                        {
                            curPageLoader.item.init()
                            curpageLoader.item.forceActiveFocus()
                        }
                    }
                }
            }
      }
}




