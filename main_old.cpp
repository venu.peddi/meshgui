#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "modelpublicationset.h"
#include "modelpublicationstatus.h"
#include "messagehandler.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    MessageHandler messageHandler;
    engine.rootContext()->setContextProperty("messageHandler", &messageHandler);

    qmlRegisterType<ModelPublicationSet>("io.qt.examples.ModelPublicationSet", 1, 0, "ModelPublicationSet");
    qmlRegisterType<ModelPublicationStatus>("io.qt.examples.ModelPublicationStatus", 1, 0, "ModelPublicationStatus");
    qmlRegisterType<MessageHandler>("io.qt.examples.MessageHandler", 1, 0, "MessageHandler");


    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    return app.exec();
}
